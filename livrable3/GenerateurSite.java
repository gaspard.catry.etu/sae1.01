class GenerateurSite extends Program {
    int IDX_NOM = 0;
    int IDX_DATE = 1;
    int IDX_ENTREPRISE = 2;
    int IDX_PRIX = 3;
    int IDX_DESCRIPTION = 4;
    final String [] mot_clef = {"nom","date","entreprise","prix","description"};
    final String html = "<!DOCTYPE html>\n" +
                        "<html lang=\"fr\">\n" +
                        "  <head>\n" +
                        "    <title>Ordinateurs mythiques</title>\n" +
                        "    <meta charset=\"utf-8\">\n" +
                        "    <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n" +
                        "  </head>\n" +
                        "  <body>\n" +
                        "    <header>\n" +
                        "      <h1>Ordinateurs mythiques</h1>\n" +
                        "    </header>\n" +
                        "    <nav>\n" +
                        "      <ul>\n" +
                        "        <li><a href=\"index.html\">Accueil</a></li>\n" +
                        "      </ul>\n" +
                        "    </nav>\n" +
                        "    <main>\n" +
                        "      <section>\n" +
                        "        <h2></h2>\n" +
                        "          <p>\n          </p>\n" +
                        "      </section>\n" +
                        "    </main>\n" +
                        "  </body>\n" +
                        "</html>\n";

    String valSearch(String txt, String name) {
        String output = "";
        int indice = 0;
        int debutIndice = 0;

        while (equals(output, "") && indice < length(txt)) {
            int iSansEspace = indice-1;
            if(txt.charAt(indice) == ':' && length(name) <= iSansEspace) {
                if(equals(substring(txt, iSansEspace-length(name), iSansEspace), name)) {
                    debutIndice = indice+2;
                }
            }
            if (txt.charAt(indice) == '\n' && debutIndice != 0) {
                output = substring(txt, debutIndice, indice);
            }
            indice = indice + 1;
        }
        return output;
    }

    String tabtoString(String[][] produits){
        String str="[[";
        for(int i = 0; i < length(produits); i++){
            for(int j = 0; j < 5; j++){
                str+=produits[i][j]+",";
            }
            str+="]\n";
        }
        str+="]";
        return str;
    }

    boolean estTrie(int[] tab) {
        boolean reponse = true;
        int n = tab.length;
        for (int indice = 0; indice < n - 1; indice++) {
            if (tab[indice] > tab[indice + 1]) {
                reponse= false;
            }
        }
        return reponse;
    }

    void triBulles(int[] tab) {
        int n = length(tab);
        do {
            for (int indice = 0; indice < n - 1; indice++) {
                if (tab[indice] > tab[indice + 1]) {
                    int temp = tab[indice];
                    tab[indice] = tab[indice + 1];
                    tab[indice + 1] = temp;
                }
            }
        } while (estTrie(tab)==false);
    }

    int [] triProduit(String [] tab){
        String numero_str, produitX;
        int numero_int;
        int [] tabNumeroProduit = new int [length(tab,1)];
        for(int indice = 0; indice < length(tab,1); indice=indice+1){
            numero_str = ""+ charAt(tab[indice], 7);
            if(charAt(tab[indice],8)-'0'>=0 && charAt(tab[indice], 8)-'0'<10){
                numero_str = numero_str + "" +charAt(tab[indice],8);
            }
            numero_int = Integer.parseInt(numero_str);
            tabNumeroProduit[indice]=numero_int;
        }
        triBulles(tabNumeroProduit);
        return tabNumeroProduit;
    }

    String[][] chargerProduits(String repertoire, String prefixe){
        String txt; 
        int [] tabNumeroProduit =triProduit(getAllFilesFromDirectory(repertoire));
        String [] tabProduitTxt = new String [length(tabNumeroProduit,1)];
        String [][] tableauProduits = new String [length(tabProduitTxt,1)][5];
        for(int fileNb = 0; fileNb < length(tabNumeroProduit,1);fileNb++){
            tabProduitTxt[fileNb]=repertoire+prefixe+tabNumeroProduit[fileNb]+".txt";
            txt = fileAsString(tabProduitTxt[fileNb]) ;
            tableauProduits[fileNb][IDX_NOM]=valSearch(txt,mot_clef[IDX_NOM]);
            tableauProduits[fileNb][IDX_DATE]=valSearch(txt,mot_clef[IDX_DATE]);
            tableauProduits[fileNb][IDX_DESCRIPTION]=valSearch(txt,mot_clef[IDX_DESCRIPTION]);
            tableauProduits[fileNb][IDX_ENTREPRISE]=valSearch(txt,mot_clef[IDX_ENTREPRISE]);
            tableauProduits[fileNb][IDX_PRIX]=valSearch(txt,mot_clef[IDX_PRIX]);
        }
        return tableauProduits;       
    }

    String txtToHtml(int fileNb,String txt, String [][] tab) {
        String output = "";
        for(int indice = 0; indice<length(html); indice++) {
            output = output + charAt(html, indice);
            
            if(charAt(html, indice) == '<') {
                if (equals(substring(html, indice, indice+3), "<p>")) {
                    output = substring(output, 0, output.length() - 3) + "<";
                } else if(equals(substring(html, indice, indice+4), "</p>")) {
                    output = substring(output, 0, output.length() - 3) + "<";
                }
            } 
            if (charAt(html, indice) == '>') {
                if (equals(substring(html, indice-2, indice+1), "<p>")) {
                    output = output + "\n" + tab[fileNb][IDX_DESCRIPTION];
                } else if(equals(substring(html, indice-3, indice+1), "<h2>")) {
                    output = output + tab[fileNb][IDX_NOM] + " (" + tab[fileNb][IDX_ENTREPRISE] + ")";
                } else if(equals(substring(html, indice-4, indice+1), "</h2>")) {
                    output = output + "\n        <h3>" + tab[fileNb][IDX_PRIX] + " (Sortie en " + tab[fileNb][IDX_DATE] + ")</h3>";
                }
            }
        }
        return output;
    }

    String indexPage() {
        String output = "";

        for(int indice = 0; indice<length(html); indice++) {
            output = output + charAt(html, indice);
            if (charAt(html, indice) == '>') {
                if(equals(substring(html, indice-4,indice+1), "</li>")){
                    output = output +
                    "\n        <li><a href=\"produit1.html\">Altair 8800</a></li>\n"+
                    "        <li><a href=\"produit2.html\">NeXT Computer</a></li>\n"+
                    "        <li><a href=\"produit3.html\">Sinclair ZX Spectrum</a></li>\n"+
                    "        <li><a href=\"produit4.html\">Amiga 1000</a></li>\n"+
                    "        <li><a href=\"produit5.html\">ENIAC</a></li>";
                    
                }else if(equals(substring(html, indice-3, indice+1), "<h2>")) {
                    output = output + "Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !";
                }else if(equals(substring(html, indice-2, indice+1), "<p>")) {
                    output = output + "\nBienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique.";
                }
            }
        }
        return output;
    }

    String addLi(int fileNb, String page, String[][]tab) {
        String output = "";
        int n = 5;
        int test = length(tab)+1-(fileNb+1);
        if(test<5){
            n=test;
        }
        if(test==0){
            n=1;
        }
        for(int indice = 0; indice<page.length()-2; indice++) {
            output = output + charAt(page, indice);
            if (charAt(page, indice) == '>') {
                if(equals(substring(page, indice-4, indice+1), "</li>")) {
                    for(int jdx = fileNb+1; jdx<=fileNb+n; jdx++) {
                        output = output + "\n        <li><a href=\"produit" + jdx + ".html\">" + tab[jdx-1][IDX_NOM] + "</a></li>";
                    }
                }
            }
        }
        
        return output;
    }

    void algorithm() {
        
        String [][] tableauProduits = chargerProduits("./data","/produit");
        //println(tabtoString(tableauProduits));
        String targetname = "./data/produit";
        String index = indexPage();
        stringAsFile("./output/index.html", index);
        for(int fileNb =1; fileNb <= length(tableauProduits,1); fileNb++){
            if(fileExist(targetname + fileNb + ".txt") && !fileExist("./output/produit" + fileNb + ".html")){
                String content = fileAsString(targetname + fileNb + ".txt");
                String html = txtToHtml(fileNb-1,content,tableauProduits);
                stringAsFile("./output/" + "produit" + fileNb + ".html", html);
                content = fileAsString("./output/" + "produit" + fileNb + ".html");
                html = addLi(fileNb-1, content,tableauProduits);
                stringAsFile("./output/" + "produit" + fileNb + ".html", html);
            }
        }
    }
}