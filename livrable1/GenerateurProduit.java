class GenerateurProduit extends Program{
    String rechercheVariable(String texte, String var){
        String valeur="";
        if( texte.contains(var)){
            int indice0 = texte.indexOf(var);
            texte= substring(texte,indice0, length(texte));
            int indice1 = texte.indexOf("\n");
            valeur = substring(texte,0,indice1) ;
            int indice2 = valeur.indexOf(":")+2;
            valeur= substring(valeur,indice2, indice1);
            texte= substring(texte,indice1+1,length(texte));
        }else{
            error("ERREUR: Variable introuvable.");
        }
        return valeur ;
    }
    void algorithm(){
        if(fileExist(argument(0))){
            String nomFichier = argument(0);
            String texte = fileAsString(nomFichier);
            String nom, date, entreprise, prix, description;
            nom = rechercheVariable( texte,"nom");
            date = rechercheVariable( texte, "date");
            entreprise = rechercheVariable( texte, "entreprise");
            prix = rechercheVariable( texte, "prix");
            description = rechercheVariable( texte, "description");
            println("<!DOCTYPE html>\n<html lang=\"fr\">\n  <head>\n    <title>"+nom+"</title>\n    <meta charset=\"utf-8\">\n  </head>\n  <body>\n    <h1>"+nom+" ("+entreprise+")"+"</h1>\n    <h2>"+prix+" (Sortie en "+date+")"+"</h2>\n    <p>\n"+description);
            println("    </p>\n  </body>\n</html>");
        }else{
        error("Y'a un problème ¯|_(ツ)_|¯");
        }
    }
}

