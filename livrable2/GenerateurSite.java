class GenerateurSite extends Program {
    final String html = "<!DOCTYPE html>\n" +
    "<html lang=\"fr\">\n" +
    " <head>\n" +
    " <title>Ordinateurs mythiques</title>\n" +
    " <meta charset=\"utf-8\">\n" +
    " <link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\">\n" +
    " </head>\n" +
    " <body>\n" +
    " <header>\n" +
    " <h1>Ordinateurs mythiques</h1>\n" +
    " </header>\n" +
    " <nav>\n" +
    " <ul>\n" +
    " <li><a href=\"index.html\">Accueil</a></li>\n" +
    " </ul>\n" +
    " </nav>\n" +
    " <main>\n" +
    " <section>\n" +
    " <h2></h2>\n" +
    " <p>\n </p>\n" +
    " </section>\n" +
    " </main>\n" +
    " </body>\n" +
    "</html>\n";

    String valSearch(String txt, String name) {
        String output = "";
        int i = 0;
        int debutIndice = 0;

        while (equals(output, "") && i < length(txt)) {
            int iSansEspace = i-1;
            if(txt.charAt(i) == ':' && length(name) <= iSansEspace) {
                if(equals(substring(txt, iSansEspace-length(name), iSansEspace), name)) {
                    debutIndice = i+2;
                }
            }
            if (txt.charAt(i) == '\n' && debutIndice != 0) {
                output = substring(txt, debutIndice, i);
            }
            i = i + 1;
        }
        return output;
    }


    String txtToHtml(String txt) {
        String output = "";
        String nom = valSearch(txt, "nom");
        String date = valSearch(txt, "date");
        String description = valSearch(txt, "description");
        String entreprise = valSearch(txt, "entreprise");
        String prix = valSearch(txt, "prix");

        for(int i = 0; i<length(html); i++) {
            output = output + charAt(html, i);
            
            if(charAt(html, i) == '<') {
                if (equals(substring(html, i, i+3), "<p>")) {
                    output = substring(output, 0, output.length() - 3) + "<";
                } else if(equals(substring(html, i, i+4), "</p>")) {
                    output = substring(output, 0, output.length() - 3) + "<";
                }
            } 
            if (charAt(html, i) == '>') {
                if (equals(substring(html, i-2, i+1), "<p>")) {
                    output = output + "\n" + description;
                } else if(equals(substring(html, i-3, i+1), "<h2>")) {
                    output = output + nom + " (" + entreprise + ")";
                } else if(equals(substring(html, i-4, i+1), "</h2>")) {
                    output = output + "\n <h3>" + prix + " (Sortie en " + date + ")</h3>";
                }
            }
        }
        return output;
    }


    String indexPage(int fileNb) {
        String output = "";

        for(int i = 0; i<length(html); i++) {
            output = output + charAt(html, i);
            if (charAt(html, i) == '>') {
                if(equals(substring(html, i-3, i+1), "<h2>")) {
                    output = output + "Tout ce que vous avez toujours voulu savoir sur les vieux ordis sans jamais avoir osé le demander !";
                } else if(equals(substring(html, i-2, i+1), "<p>")) {
                    output = output + "\nBienvenue dans le musée virtuel d'ordinateurs mythiques de l'histoire de l'informatique. Vous trouverez ici des éléments sur quelques machines qui ont marqué l'histoire de l'informatique que cela soit par leurs caractéristiques techniques ou l'impact commercial qu'elles ont eu et qui ont contribué au développement du secteur informatique.";
                }
            }
        }
        return output;
    }

    String addLi(int fileNb, String page) {
        String output = "";
        for(int i = 0; i<page.length()-2; i++) {
            output = output + charAt(page, i);
            if (charAt(page, i) == '>') {
                if(equals(substring(page, i-4, i+1), "</li>")) {
                    for(int j = 1; j<fileNb; j++) {
                        output = output + "\n <li><a href=\"produit" + j + ".html\">Produit " + j + "</a></li>";
                    }
                }
            }
        }
        output += ">";
        return output;
    }

    void algorithm() {
        String targetname = "./data/produit";
        int fileNb = 1;
        while(fileExist(targetname + fileNb + ".txt") && !fileExist("./output/produit" + fileNb + ".html")) {
            String content = fileAsString(targetname + fileNb + ".txt");
            String html = txtToHtml(content);
            stringAsFile("./output/" + "produit" + fileNb + ".html", html);
            fileNb = fileNb + 1;
        }


        for(int i = 1; i<fileNb; i++) {
            String content = fileAsString("./output/" + "produit" + i + ".html");
            String html = addLi(fileNb, content);
            stringAsFile("./output/" + "produit" + i + ".html", html);
        }
        String index = indexPage(fileNb);
        String indexLi = addLi(fileNb, index) + "\n";
        stringAsFile("./output/index.html", indexLi);

    }
}